<?php
session_start();

    $jsonBlob = '[
        {
            "top_text":"Select all squares representing threats to Journalists",
            "bottom_text":"Google has a partnership with the United States Department of Defense to help the agency develop artificial intelligence for analyzing drone footage...The United States Department killed journalists and civillians in the war in Iraq...",
            "picture": "https://coletivos.org/fuckoffgoogle/img/journalist.png"
        },
        {
            "top_text":"Select all squares with penguins.",
            "bottom_text":"Aren\'t you tired of this \"I\'m not a robot\" dialogs from google? Do you known that they are using this training for developing computer vision artificial intelligence for drones?",
            "picture": "http://www.jigsaw-puzzle-club.co.uk/Jigsaws/O59609-King%20Penquinsc%20Square%20Jigsaw%20Puzzle-w.jpg"
        },
        {
            "top_text":"Select all squares with GPL licenses",
            "bottom_text":"Did you known that google hates AGPL? Use AGPL if you want to avoid google from using your software for killing people",
            "picture": "https://www.whitesourcesoftware.com/wp-content/uploads/2015/07/GPL-e1435820834468.jpg"
        }
        ]';
    
$options = json_decode($jsonBlob, true);
if (!isset($_SESSION['id']) ) {
    $idx = rand(0,count($options)-1);
}else {
    $idx = $_SESSION['id'];
    $idx++;
    
    if ($idx > count($options) - 1)
    {
        $idx = 0;
    }
}
$_SESSION['id'] = $idx;

$image = $options[$idx]['picture'];
$threat = $options[$idx]['top_text'];
$text = $options[$idx]['bottom_text'];

$url = 'https://fuckoffgoogle.de';

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Attention Required! | CloudFlare</title>
		<meta charset="UTF-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
		<meta name="robots" content="noindex, nofollow" />
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />
		<link rel="stylesheet" href="./css/bootstrap.min.css" type="text/css" media="screen,projection" />
		<link rel="stylesheet" href="./css/main.css" type="text/css" media="screen,projection" />
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
		<script type="text/javascript" src="./js/jquery-2.2.2.min.js"></script>
		<script type="text/javascript" src="./js/bootstrap.min.js"></script>
		<script type="text/javascript">
$(document).ready(function() {
	// So, we haz script
	$('.script').show();

	$('#checkbox-captcha').click(function(event) {
		event.preventDefault();
		$('.captcha-modal').modal('show');
	});
	$('td.captcha').click(function(event) {
		$(this).toggleClass('checked')
	});
	$('#modal-collapse').click(function(event) {
		$('#footer-standard').hide();
		$('#footer-report').show();
		$('#footer-report').collapse('toggle');
		$('.captcha-modal').animate({"height": "725px"});
	});
	$('#footer-close').click(function(event) {
		$('.captcha-modal').animate({"height": "600px"});
		$('#footer-report').collapse('toggle');
		$('#footer-report').hide();
		$('#footer-standard').show();
	});
	$('.threat-action').change(function(event) {
		$('#redbutton').attr("href", $(this).attr("value"));
		$('#redbutton').removeClass('disabled');
	});
	$('#verify').click(function(event) {
		$('.captcha-modal').modal('hide');
		$('#checkbox-captcha').prop("checked", true);
		var timer = window.setTimeout(function() {
			document.cookie="captcha=True";
			window.location = "<?php echo $url; ?>";
		}, 1000);
	});
    $('.captcha.modal-body').css("background-image", "url('<?php echo $image; ?>')");  
    //$('.captcha-modal').modal('show');
});
		</script>
	</head>
	<body>
		<div class="container-fluid wrapper">
			<div class="row header">
				<div class="col-xs-10 col-xs-offset-1">
					<h1>One more step</h1>
					<h2><span>Please complete the security check to access</span> <?php echo $url; ?></h2>
				</div>
			</div>
			<div class="row content">
				<noscript>
					<div class="col-md-5 col-md-offset-1 col-sm-12">
						<div class="container-fluid modal captcha-modal">
							<div class="row captcha modal-header">
								<div class="col-xs-9">
									<?php echo $threat ?>
								</div>
								<div class="col-xs-3">a
									<img src="./img/logo_48.png">
								</div>
							</div>
							<div class="row captcha modal-body">
								<table class="captcha">
									<tr>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
									</tr>
									<tr>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
									</tr>
									<tr>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
									</tr>
									<tr>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
										<td class="captcha">
											<input type="checkbox">
										</td>
									</tr>
								</table>
							</div>
							<div class="row captcha modal-footer">
								<div class="col-xs-12">
									<a role="button" href="https://laquadrature.net/index.php" class="btn btn-primary btn-block">Verify</a>
								</div>
							</div>
						</div>
					</div>
				</noscript>
				<div class="script">
					<div class="col-md-5 col-md-offset-1 col-sm-12">
						<div class="recaptcha row">
							<div class="col-xs-8 form-group">
								<div class="col-xs-9 check">
									<input type="checkbox" id="checkbox-captcha">
									<label id="checkbox-captcha-label" for="checkbox-captcha"> I'm not a robot </label>
								</div>
								<div class="col-xs-3 terms">
									<img src="./img/logo_48.png">
									<h5>imsiCAPTCHA</h5>
									<h6>Privacy - Terms</h6>
								</div>
								<div class="modal fade captcha-modal" tabindex="-1" role="dialog" aria-labelledby="captcha-modal-label" data-backdrop="false">
									<div class="container-fluid">
										<div class="row captcha modal-header">
											<div class="col-xs-12">
											    <?php echo $threat ?>
											</div>
										</div>
										<div class="row captcha modal-body">
											<table class="captcha">
												<tr>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
												</tr>
												<tr>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
												</tr>
												<tr>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
												</tr>
												<tr>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
													<td class="captcha"></td>
												</tr>
											</table>
										</div>
                                        <div class="center-block">
                                            <h4><?php echo $text; ?></h4>
                                        </div>
                                        <br/>
										<div class="modal-footer" id="footer-standard">
											<div class="button-group pull-left">
												<button class="btn btn-default btn-control"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span></button>
												<button class="btn btn-default btn-control"><span class="glyphicon glyphicon-headphones" aria-hidden="true"></button>
												<button class="btn btn-default btn-control"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></button>
											</div>
                                            
											<button class="btn btn-primary" id="verify">Verify</button>
                                            <br/>
											<button class="btn btn-link btn-control pull-left" id="modal-collapse">Report a freedom violation</button>
										</div>
										<div class="collapse modal-footer" id="footer-report">
											<a href="#" role="button" class="btn btn-block btn-danger disabled" id="redbutton">REPORT NEW THREAT</a>
											<hr />
											<button class="close" id="footer-close" aria-label="Close">&times;</button>
											<h4>What is the new threat?</h4>
											<input type="radio" name="action" id="action1" class="threat-action" value="https://respectmynet.eu/"></input><label for="action1">Net neutrality violation?</label>
											<input type="radio" name="action" id="action2" class="threat-action" value="https://piphone.lqdn.fr/"></input><label for="action2">A new law in the making?</label>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 hidden-sm hidden-xs">
					<span class="screenshot">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" src="<?php echo $url;?>"></iframe>
                        </div>
                        <!--
						<img class="browser-bar img-responsive" src="./img/browser-bar.png">
						<img class="browser-screenshot img-responsive" src="./img/lqdn-crop.png">-->
					</span>
				</div>
			</div>
			<!--<div class="row footer">
				<div class="col-md-5 col-xs-10 col-md-offset-1 col-xs-offset-1">
					<h2>Google office and startup speculation in Kreuzberg</h2>
					<p>Google is trying to open a 2500m² “campus” in Kreuzberg to attract, detect and buy profitable companies and ideas.</p>
					<p>Sold as a “community” project, in reality it aims at attracting “entrepreneurs” who will increase Google’s profit.</p>
                    <p>This project will turn the neighborhood into a large-scale laboratory for the deployment of their new invasive technologies.</p>
                    <p>Instead of a nice friendly “campus” we see a Google farm for harvesting Kreuzberg’s brains and talents, or a Google mine in which ideas and data will be extracted out of Berlin.</p>
                    <a href="http://www.dw.com/en/berlin-residents-reject-google-campus/a-38586077">Learn more about the Campus</a>
				</div>
				<div class="col-md-5 col-xs-10 col-xs-offset-1">
					<h2>G-entrification</h2>
					<p>The presence of a “Google campus” in our neighborhood will accelerate its rampant gentrification making life more difficult for locals.</p>
					<p><h3>RENTS</h3>
                        “Google campus” in Kreuzberg will inevitably cause rents to increase more, pushing out long-time residents and businesses.</p>
				</div>
			</div>
			<div class="row footer">
				<div class="col-xs-10 col-xs-offset-2">
					<hr/>
					<p>
					<span>ImsiCaptcha Ray ID: <strong>28494950ee9208f0</strong></span>
					<span>&bull;</span>
					<span>Your IP: 127.0.0.1</span>
					<span>&bull;</span>
					<span>Non-Performance &amp; security by</span> <a "https://git.laquadrature.net/okhin/imsiCAPTCHA" target="_blank">La Quadrature du Net</a>
					</p>
				</div>
			</div>-->
		</div>
	</body>
</html>
